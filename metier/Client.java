package metier;
import java.util.Set;
import java.util.HashSet;
/**
 * La classe Client représente le client d'une banque.
 *
 * @author V. Britelle
 * @version 22-oct-2020
 */
public class Client
{
    private static Set<Client> clients = new HashSet<Client>();
    private String nom;

    /**
     * Crée un client.
     */
    public Client(String nom)
    {
        this.nom = nom;
        // Ajout de ce nouveau client à la liste de tous les clients
        // conservée au niveau de la classe (static)
        clients.add(this);
    }

    public String getNom()
    {
        return nom;
    }
    
    public void setNom(String nom)
    {
        this.nom = nom;
    }
    
    public static Set<Client> getClients()
    {
        return clients;
    }
    
    @Override
    public String toString()
    {
        return nom;
    }
}
