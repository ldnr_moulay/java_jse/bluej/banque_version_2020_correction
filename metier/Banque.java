package metier;

/**
 * La classe Banque représente un établissement
 * qui gère des comptes bancaires.
 *
 * @author V. Britelle
 * @version 24-oct-2020
 */
public class Banque
{
    private String nom;
    private String adresse;
    

    /**
     * Constructeur d'objets de classe Banque
     */
    public Banque(String nom, String adresse)
    {
        this.nom = nom;
        this.adresse = adresse;
    }

    public String getNom()
    {
        return nom;
    }
    
    public String getAdresse()
    {
        return adresse;
    }
    
    public void setNom(String nom)
    {
        this.nom = nom;
    }
    
    @Override
    public String toString()
    {
        return "La banque " + nom
            + " est domiciliée à " + adresse + ".";
    }
}
