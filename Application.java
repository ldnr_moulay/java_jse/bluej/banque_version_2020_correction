import menu.*;
import commande.*;
import io.InputReader;

public class Application
{
    InputReader inputReader = new InputReader();
    
    /**
     * Boucle: affiche menu,récupère souhait, réalise le souhait
     */
    public void start()
    {
        String souhait;
        Menu menu = new Menu();
        initMenu(menu);
        boolean fini = false;
        while (!fini)
        {
            menu.afficher();
            System.out.println("-> Q pour quitter");
            souhait = inputReader.getInput();
            if (souhait.toLowerCase().equals("q"))
                fini = true;
            else
                menu.realise(souhait);
        }
        System.out.println("Au plaisir de vous revoir.");
    }
    
    private void initMenu(Menu menu)
    {
        menu.ajouter("a",
            new MenuItem("Lister les client", new ListerClientsCmd()));
        menu.ajouter("b",
            new MenuItem("Ajouter un client", new CreerClientCmd()));
    }
}
