package menu;

import commande.Commande;

/**
 *
 * @author Vincent
 */
public class MenuItem
{
    private Commande commande;
    private String label;
    
    public MenuItem(String label, Commande commande)
    {
        this.label = label;
        this.commande = commande;
    }
    
    public String getLabel()
    {
        return label;
    }
    
    public Commande getCommande()
    {
        return commande;
    }
}
